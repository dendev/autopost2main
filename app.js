const puppeteer = require('puppeteer');
const fs = require('fs');

const config = require('./config.json');

(async () => {
    const browser = await puppeteer.launch({headless:false});
    const page = await browser.newPage();
    //await page.setViewport({ width: 1366, height: 968});

    await page.goto( config.url );
    await page.waitForSelector( config.selectors.login.form )

    // login
    console.info( 'login with username && password' )
    await page.type( config.selectors.login.username, config.values.username )
    await page.type( config.selectors.login.password, config.values.password )
    await page.click( config.selectors.login.submit )

    // go to add
    console.info( 'go to add page like normal user ( but more speedly )' )
    await page.waitForSelector( config.selectors.add.url )
    await page.click( config.selectors.add.url )

    // read all adverts to publish
    fs.readdir('./adverts', async function(err, adverts) {

        for (var i=0; i < adverts.length ; i++)
        {
            console.info( `create advert for ${adverts[i]}` )
            let ad_path = `./adverts/${adverts[i]}/`
            let values = require(`${ad_path}/values.json`)
            config.values = Object.assign( config.values, values );

            // create new advert
            await page.waitForSelector( config.selectors.add.form )

            //    // title
            console.info( '-- title' )
            await page.focus( config.selectors.add.title )
            await page.keyboard.type( config.values.title, { delay: 250 } ); // slower because i m an user ! -> let's time to generate category

            //    // category
            console.info( '-- category' )
            await page.click( config.selectors.add.category, config.values.category )

            //    // description
            console.info( '-- description' )
            await page.type( config.selectors.add.description, config.values.description )

            //    // price ( <amount>, gratis, ruilen, teab, nvt )
            console.info( '-- price' )
            await page.waitForSelector( config.selectors.add.price )
            await page.waitFor( 1000 )

            let price_value = ( config.values.price == 'amount' ) ? "\<amount\>" : config.values.price
            await page.select( config.selectors.add.price,  price_value )
            await page.waitFor( 1000 )
            if( config.values.price == 'gratis' )
            {
                console.info( '-- -- gratis' )
                // do nothing
            }
            else if( config.values.price == 'amount' )
            {
                console.info( '-- -- payant' )
                await page.type( config.selectors.add.amount, config.values.amount )
                await page.waitFor( 2000 )
                if( config.values.allow_offering ) // enabled by default
                {
                    console.info( '-- -- -- allow offering' )
                    await page.waitForSelector( config.selectors.add.default_offering )
                    await page.type( config.selectors.add.default_offering, config.values.default_offering )
                }
                else
                {
                    console.info( '-- -- -- disable offering' )
                    await page.waitForSelector( config.selectors.add.allow_offering )
                    await page.click( config.selectors.add.allow_offering )
                }
            }
            else
            {
                console.info( '-- -- others' )
            }

            //    // delivery ( home, send )
            console.log( '-- delivery' )
            if( config.values.delivery == "home" )
                await page.click( config.selectors.add.delivery.home )
            else
                await page.click( config.selectors.add.delivery.send ) // not implemented yet ( because i don't care )

            //    // state
            let state_value = ( config.values.state == 'old' ) ? 'conditie/gebruikt' : 'conditie/nieuw';
            await page.select( config.selectors.add.state,  state_value )

            //    // website
            if( config.values.website != "false" )
                await page.type( config.selectors.add.website , config.values.website )

            //    // photos
            fs.readdir(`${ad_path}/imgs`, async function(err, imgs) {
                for (var j = 0; j < imgs.length; j++)
                {
                    let path = `${ad_path}/imgs/${imgs[j]}`
                    console.log( path )
                    const input = await page.$( config.selectors.add.photos )
                    await input.uploadFile(path);
                    await page.waitFor( 5000 )
                }
            })

            // submit
            await page.click( config.selectors.add.submit )
        }
    });


    //  await browser.close();
})();

