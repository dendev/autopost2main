# AutoPost2main

*post des annonces sur second main*

## Installation

Utilise [https://pptr.dev/](puppeteer) avec un chromium embarquer

```bash
npm update 
```

## Structure 

```text
.
├── adverts
│   └── sac-1 --> mon annonce
│       ├── imgs --> ses images 
│       │   └── img1.jpg 
│       └── values.json --> les valeurs de l'annonces
├── app.js --> spider
├── config.json --> config du spider ( selecteurs && valeurs user, url, ... )
├── package.json
└── Readme.md

3 directories, 6 files

```

## Création d'une annonce

Dans adverts 
```bash
chmod u+x add_advert.sh && ./add_advert.sh sac-1
```

Créer un répertoire avec son emplacement pour les photos et son fichier de config.   

Fichier de config

```json
{
    "title": "sac de randonée 30L",
    "description": "test",
    "price": "amount",
    "amount": "35",
    "allow_offering": "true",
    "default_offering": "30",
    "delivery": "home",
    "state": "old",
    "website": "false"
} 
```
L'ajout de photos se fait par le script en lisant le contenu du repertoire imgs
