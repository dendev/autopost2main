#!/bin/bash

values_default='
{\n
    \t"title": "sac de randonée 30L",\n
    \t"description": "test",\n
    \t"price": "amount",\n
    \t"amount": "35",\n
    \t"allow_offering": "true",\n
    \t"default_offering": "30",\n
    \t"delivery": "home",\n
    \t"state": "old",\n
    \t"website": "false"\n
}'


echo -e "Create dir for $1"
mkdir adverts/$1 && cd adverts/$1 && mkdir imgs

echo -e "Create config file"
echo -e $values_default > values.json
vim values.json

exit $?
